-- This file is part of SCADA Plastic rolling machine.
-- 
-- Copyright 2018, Prointegra SL.
--
-- SCADA Plastic rolling machine is free software: you can redistribute it and/or modify it under the #terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SCADA Plastic rolling machiner is distributed in the hope that it will 
-- be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SCADA Plastic rolling machine.  
-- If not, see <https://www.gnu.org/licenses/>.

CREATE TABLE USERS(ID INT PRIMARY KEY,NAME VARCHAR(20),PASSWORD VARCHAR(20),permission INT);

INSERT INTO USERS (NAME,PASSWORD,permission) values
("admin", "123456", 0),
("user1", "1234", 6),
("user2", "1234", 6),
("user3", "1234", 6),
("user4", "1234", 6),
("user5", "1234", 6),
("user6", "1234", 6),
("user7", "1234", 6),
("user8", "1234", 6),
("user9", "1234", 6),
("user10", "1234", 6),
("support", "support_password", 3);
